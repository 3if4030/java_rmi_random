package if4030.rmi;
        
import java.rmi.registry.Registry;
import java.rmi.registry.LocateRegistry;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
        
public class RandomServer implements IRandom {
    private int min = 1;
    private int max = 100;
        
    public void setBounds( int min, int max ) throws RemoteException {
        if( min <=max ) {
            this.min = min;
            this.max = max;
        }
    }
    
    public int nextRandom() throws RemoteException {
        double alea = Math.random();
        return min + ( int ) (( max - min + 1 ) * alea );
    }

    public static void main( String args[] ) {
        
        try {
            RandomServer random = new RandomServer();
            IRandom stub = ( IRandom ) UnicastRemoteObject.exportObject( random, 0 );
            
            LocateRegistry.createRegistry( 10099 );
            Registry registry = LocateRegistry.getRegistry( "localhost", 10099 );
            registry.bind( "Random", stub );

            System.err.println( "Server ready" );
        }
        catch (Exception e) {
            System.err.println( "Server exception: " + e.toString() );
            e.printStackTrace();
        }
    }
}
