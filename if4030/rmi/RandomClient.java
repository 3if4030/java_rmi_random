package if4030.rmi;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class RandomClient {

    public static void main( String[] args ) {

        try {
            Registry registry = LocateRegistry.getRegistry( "localhost", 10099 );
            IRandom random = ( IRandom ) registry.lookup( "Random" );
            if( args.length >= 2 ) {
                random.setBounds( Integer.valueOf( args[0] ), Integer.valueOf( args[1] ));
            }
            else {
                for( int i = 0; i < 10; ++i ) {
                    System.out.print( random.nextRandom() + " " );
                }
                System.out.println();
            }
        }
        catch ( Exception e ) {
            System.err.println( "Client exception: " + e.toString() );
            e.printStackTrace();
        }
    }
}
